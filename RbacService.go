package rbac

import (
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/valyala/fasthttp"
)

type headerGetter = func(ctx *fasthttp.RequestCtx) string

// Сервис проверки доступа пользователя и авторизации
type rbacService struct {
	permissionLoader TPermissionLoader
	jwtPassword      []byte
	headerGetter     headerGetter
}

// Конструктор сервиса
func NewRbacService(
	permissionLoader TPermissionLoader,
	jwtPassword string,
) RbacServiceInterface {
	return &rbacService{
		permissionLoader: permissionLoader,
		jwtPassword:      []byte(jwtPassword),
		headerGetter: func(ctx *fasthttp.RequestCtx) string {
			return string(ctx.Request.Header.Peek("Authorization"))
		},
	}
}

// Получение данных авторизации пользователя из параметров запроса
func (r rbacService) GetAuthFromRequest(ctx *fasthttp.RequestCtx) (*Authorization, error) {
	token := r.headerGetter(ctx)
	if 0 == len(token) {
		return nil, fmt.Errorf(`token is not send by 'Authorization' header`)
	}

	return r.GetAuthByToken(token)
}

// Получение данных авторизации пользователя по переданному токену
func (r rbacService) GetAuthByToken(token string) (*Authorization, error) {
	var auth Authorization
	tokenParse, err := jwt.ParseWithClaims(token, &auth, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}

		return r.jwtPassword, nil
	})

	if nil != err {
		return nil, fmt.Errorf(`incorrect token: %v`, err.Error())
	}

	if !tokenParse.Valid {
		return nil, fmt.Errorf(`token is not valid`)
	}

	return &auth, nil
}

// Шифрование данных авторизации в JWT токен
func (r rbacService) EncryptToken(authorization Authorization) string {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, authorization)
	ss, _ := token.SignedString(r.jwtPassword)

	return ss
}

// Проверка прав доступа пользователя по переданному коду
func (r rbacService) CheckPermission(authorization Authorization, permission string) bool {
	permissionsMap := map[int]Permission{}
	for _, permission := range r.permissionLoader() {
		permissionsMap[permission.Id] = permission
	}

	for _, permissionId := range authorization.Permissions {
		permissionObject := permissionsMap[permissionId]
		if permissionObject.Code == permission {
			return true
		}
	}

	return false
}
