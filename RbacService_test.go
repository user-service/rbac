package rbac

import (
	"github.com/dgrijalva/jwt-go"
	"github.com/valyala/fasthttp"
	"reflect"
	"testing"
)

// Тестирование генерации токена
func Test_rbacService_EncryptToken(t *testing.T) {
	type fields struct {
		permissionLoader TPermissionLoader
		jwtPassword      []byte
	}
	type args struct {
		authorization Authorization
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   string
	}{
		{
			name: "Тестирование генерации токена",
			fields: fields{
				permissionLoader: func() []Permission {
					return nil
				},
				jwtPassword: []byte("test_pwd"),
			},
			args: args{
				authorization: Authorization{
					UserId:      1,
					Roles:       []int{1, 2},
					Permissions: []int{3, 5, 8},
					StandardClaims: jwt.StandardClaims{
						ExpiresAt: 0,
						Issuer:    "user",
					},
				},
			},
			want: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoxLCJyb2xlcyI6WzEsMl0sInBlcm1pc3Npb25zIjpbMyw1LDhdLCJpc3MiOiJ1c2VyIn0.7e5osRWYHp1I8N_GUf-3U9_5ya_h_jRGS17jN4601AE",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := rbacService{
				permissionLoader: tt.fields.permissionLoader,
				jwtPassword:      tt.fields.jwtPassword,
			}
			if got := r.EncryptToken(tt.args.authorization); got != tt.want {
				t.Errorf("EncryptToken() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование авторизации пользователя по токену
func Test_rbacService_GetAuthFromRequest(t *testing.T) {
	type fields struct {
		permissionLoader TPermissionLoader
		jwtPassword      []byte
		headerGetter     headerGetter
	}
	type args struct {
		ctx *fasthttp.RequestCtx
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *Authorization
		wantErr bool
	}{
		{
			name: "Токен не передан",
			fields: fields{
				permissionLoader: func() []Permission {
					return nil
				},
				jwtPassword: []byte("test_pwd"),
				headerGetter: func(ctx *fasthttp.RequestCtx) string {
					return ""
				},
			},
			args: args{
				ctx: &fasthttp.RequestCtx{},
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Токен не корректен",
			fields: fields{
				permissionLoader: func() []Permission {
					return nil
				},
				jwtPassword: []byte("test_pwd"),
				headerGetter: func(ctx *fasthttp.RequestCtx) string {
					return "ваыа234авыа"
				},
			},
			args: args{
				ctx: &fasthttp.RequestCtx{},
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Токен корректен",
			fields: fields{
				permissionLoader: func() []Permission {
					return nil
				},
				jwtPassword: []byte("test_pwd"),
				headerGetter: func(ctx *fasthttp.RequestCtx) string {
					return "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoxLCJyb2xlcyI6WzEsMl0sInBlcm1pc3Npb25zIjpbMyw1LDhdLCJpc3MiOiJ1c2VyIn0.7e5osRWYHp1I8N_GUf-3U9_5ya_h_jRGS17jN4601AE"
				},
			},
			args: args{
				ctx: &fasthttp.RequestCtx{},
			},
			want: &Authorization{
				UserId:      1,
				Roles:       []int{1, 2},
				Permissions: []int{3, 5, 8},
				StandardClaims: jwt.StandardClaims{
					ExpiresAt: 0,
					Issuer:    "user",
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := rbacService{
				permissionLoader: tt.fields.permissionLoader,
				jwtPassword:      tt.fields.jwtPassword,
				headerGetter:     tt.fields.headerGetter,
			}
			got, err := r.GetAuthFromRequest(tt.args.ctx)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetAuthFromRequest() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetAuthFromRequest() got = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование проверки прав доступа
func Test_rbacService_CheckPermission(t *testing.T) {
	type fields struct {
		permissionLoader TPermissionLoader
		jwtPassword      []byte
		headerGetter     headerGetter
	}
	type args struct {
		authorization Authorization
		permission    string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			name: "Проверка отсутствующего права",
			fields: fields{
				permissionLoader: func() []Permission {
					return []Permission{
						{
							Id:   1,
							Code: "test-1",
						},
						{
							Id:   2,
							Code: "test-2",
						},
						{
							Id:   3,
							Code: "test-3",
						},
					}
				},
				jwtPassword:  []byte("test_pwd"),
				headerGetter: nil,
			},
			args: args{
				authorization: Authorization{
					UserId:         1,
					Roles:          nil,
					Permissions:    []int{1, 2},
					StandardClaims: jwt.StandardClaims{},
				},
				permission: "test-3",
			},
			want: false,
		},
		{
			name: "Проверка неизвестного права",
			fields: fields{
				permissionLoader: func() []Permission {
					return []Permission{
						{
							Id:   1,
							Code: "test-1",
						},
						{
							Id:   2,
							Code: "test-2",
						},
					}
				},
				jwtPassword:  []byte("test_pwd"),
				headerGetter: nil,
			},
			args: args{
				authorization: Authorization{
					UserId:         1,
					Roles:          nil,
					Permissions:    []int{1, 2},
					StandardClaims: jwt.StandardClaims{},
				},
				permission: "test-3",
			},
			want: false,
		},
		{
			name: "Проверка имеющегося права",
			fields: fields{
				permissionLoader: func() []Permission {
					return []Permission{
						{
							Id:   1,
							Code: "test-1",
						},
						{
							Id:   2,
							Code: "test-2",
						},
					}
				},
				jwtPassword:  []byte("test_pwd"),
				headerGetter: nil,
			},
			args: args{
				authorization: Authorization{
					UserId:         1,
					Roles:          nil,
					Permissions:    []int{1, 2},
					StandardClaims: jwt.StandardClaims{},
				},
				permission: "test-1",
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := rbacService{
				permissionLoader: tt.fields.permissionLoader,
				jwtPassword:      tt.fields.jwtPassword,
				headerGetter:     tt.fields.headerGetter,
			}
			if got := r.CheckPermission(tt.args.authorization, tt.args.permission); got != tt.want {
				t.Errorf("CheckPermission() = %v, want %v", got, tt.want)
			}
		})
	}
}
