module bitbucket.org/user-service/rbac

go 1.14

require (
	github.com/andybalholm/brotli v1.0.1 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/klauspost/compress v1.11.6 // indirect
	github.com/valyala/fasthttp v1.19.0
)
