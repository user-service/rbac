package rbac

import (
	"github.com/valyala/fasthttp"
)

// Сервис проверки доступа пользователя и авторизации
type RbacServiceInterface interface {
	// Получение данных авторизации пользователя из параметров запроса
	GetAuthFromRequest(ctx *fasthttp.RequestCtx) (*Authorization, error)

	// Получение данных авторизации пользователя по переданному токену
	GetAuthByToken(token string) (*Authorization, error)

	// Шифрование данных авторизации в JWT токен
	EncryptToken(authorization Authorization) string

	// Проверка прав доступа пользователя по переданному коду
	CheckPermission(authorization Authorization, permission string) bool
}
