package rbac

import jwt "github.com/dgrijalva/jwt-go"

// Данные авторизации пользователя
type Authorization struct {
	UserId      int   `json:"user_id"`
	Roles       []int `json:"roles"`
	Permissions []int `json:"permissions"`
	jwt.StandardClaims
}

// Разрешение
type Permission struct {
	Id   int
	Code string
}

type TPermissionLoader = func() []Permission
